package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    /**
     * Creates a new ExerciseFivePanel.
     */

    private JButton addNumberButton;
    private JButton subtractNumberButton;

    private JTextField firstTextField;
    private JTextField secondTextField;
    private JTextField resultTextField;

    private JLabel resultLabel;


    public ExerciseTwoPanel() {
        setBackground(Color.white);
        addNumberButton = new JButton("Add");
        subtractNumberButton = new JButton("Subtract");

        firstTextField = new JTextField(10);
        secondTextField = new JTextField(10);
        resultTextField = new JTextField(20);

        resultLabel = new JLabel("Result:");

        this.add(firstTextField);
        this.add(secondTextField);
        this.add(addNumberButton);
        this.add(subtractNumberButton);
        this.add(resultLabel);
        this.add(resultTextField);

        addNumberButton.addActionListener(this);
        subtractNumberButton.addActionListener(this);


    }

    public void actionPerformed(ActionEvent event) {
        double firstNumber = Double.parseDouble(firstTextField.getText());
        double secondNumber = Double.parseDouble(secondTextField.getText());

        if (event.getSource() == addNumberButton){
            double resultNum = roundTo2DecimalPlaces((firstNumber + secondNumber));
            resultTextField.setText(Double.toString(resultNum));
        }else if (event.getSource() == subtractNumberButton){
            double resultNum = roundTo2DecimalPlaces((firstNumber - secondNumber));
            resultTextField.setText(Double.toString(resultNum));
        }
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}