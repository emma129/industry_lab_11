package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;


/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements KeyListener, ActionListener{

    private List<Balloon> ballonBox = new ArrayList<>();
    private  Timer timer;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        ballonBox.add(new Balloon(30, 60));
        //this.balloon = new Balloon(30, 60);

        this.timer = new Timer(200, this);

        this.addKeyListener(this);


    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (Balloon balloons: ballonBox) {
            if (e.getSource() == timer) {
                balloons.move();
            }


            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
            requestFocusInWindow();
            repaint();
        }

    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon balloons : ballonBox) {
            balloons.draw(g);

            // Sets focus outside of actionPerformed so key presses work without pressing the button
            requestFocusInWindow();
        }
    }


    @Override
    public void keyPressed(KeyEvent e) {
        for (Balloon balloons : ballonBox) {

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                balloons.setDirection(Direction.Up);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                balloons.setDirection(Direction.Right);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                balloons.setDirection(Direction.Left);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                balloons.setDirection(Direction.Down);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                timer.stop();
            }

        }

        if (e.getKeyCode() == KeyEvent.VK_SPACE){

            ballonBox.add(new Balloon(((int) ((Math.random() * 200) + 100)),(int)(Math.random() * 200) + 100));
            repaint();

        }
    }
    @Override
    public void	keyReleased(KeyEvent e){

    }
    @Override
    public void	keyTyped(KeyEvent e){

    }




}